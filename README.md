# HOP
HbbTV Open Platform - A TypeScript platform for creating fast and optimized HbbTV apps

## To install
clone the repo and run `npm install`.

## To run
For development, run `npm run start` and then open `localhost:3000`.
For production, run `npm run build:prod` and copy the contents of `dist` folder contents to your web server.

## License

The MIT License (MIT)

Copyright (c) 2018-2019 Farid Roshan

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.


## Automated Tests
This application is being tested using BrowserStack.

<a href="https://www.browserstack.com" target="_blank" title="BrowserStack">
	<img width="300" src="https://p14.zdusercontent.com/attachment/1015988/T9xPfWK3jeCpq4jAw3Eb0xo66?token=eyJhbGciOiJkaXIiLCJlbmMiOiJBMTI4Q0JDLUhTMjU2In0..SmZuUZvaL7M8qtX6AXUc9g.ScXz5PUZKRwpR2qogyEEjMSHtqCIH0iXktOC5_D4qSrkGBHj1p-wjy0xvs9eB7jpF3-S9w5d8rKo6BR2GIpQG_r9h9WK7UTrl3xT51eHDeXyVFL3TdpMRgIpFkceZzs2drLBJN8C2ubHhU0t2F9i2rJPuCXOoVTjIT9rXbGbLd5DiHcOjrG20hV_vY1e6QSJa5hDx5fniFfMKpchGtssdMg4zVIsyHBFdvBiAlQhmRBQD31FmkMK2ESOm0-qGcfRdQTTyq8N8L5bWAD7epPZCbWxMp3aGdt0fyr0TBYEzv4.tJM-NnNqZx0tewAF6cC1tA" />
	</a>
